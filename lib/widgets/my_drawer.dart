import 'package:anydelivery_seller/mainScreens/homescreen.dart';
import 'package:flutter/material.dart';

import '../authentication/auth_screen.dart';
import '../global/global.dart';

class MyDrawer extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          // Drawer header
          Container(
            padding: const EdgeInsets.only(top: 25, bottom: 10),
            child: Column(
              children:  [
                //Circle Avator to display
                Material(
                  borderRadius: const BorderRadius.all(Radius.circular(70)),
                  elevation: 10,
                  child: Padding(
                    padding: EdgeInsets.all(1.0),
                    child: Container(
                      height: 160,
                      width: 160,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                          sharedPreferences!.getString("photoUrl")!
                        ),
                      ),
                    ),

                  ),
                ),
                 const SizedBox(height: 10,),
                Text(sharedPreferences!.getString("name")!,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontFamily: "Train"

                ),),

              ],
            ),
          ),
          const SizedBox(height: 12,),
          // Drawer Body
          Container(
            padding: const EdgeInsets.only(top: 10), // Padding between drawer header and body
            child: Column(
              children: [
                const Divider(
                  height: 10,
                  color: Colors.grey, // Divider Color
                  thickness: 2,
                ),
                ListTile(
                  leading:const Icon(
                    Icons.home,
                    color: Colors.black,
                  ),
                  title: const Text(
                    "Home",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Acme" ,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (c) => HomeScreen()));
                  },
                ), // go to Home Page
                ListTile(
                  leading:const Icon(
                    Icons.reorder,
                    color: Colors.black,
                  ),
                  title: const Text(
                    "New Orders",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Acme" ,
                    ),
                  ),
                  onTap: () {
                    //redirects user to homepage
                  },
                ), //New Orders
                ListTile(
                  leading:const Icon(
                    Icons.local_shipping,
                    color: Colors.black,
                  ),
                  title: const Text(
                    "History",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Acme" ,
                    ),
                  ),
                  onTap: () {
                    //redirects user to homepage
                  },
                ), // History of Orders
                ListTile(
                  leading:const Icon(
                    Icons.money,
                    color: Colors.black,
                  ),
                  title: const Text(
                    "Earnings",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Acme" ,
                    ),
                  ),
                  onTap: () {
                    //redirects user to homepage
                  },
                ), // Earnings
                ListTile(
                  leading:const Icon(
                    Icons.logout,
                    color: Colors.black,
                  ),
                  title: const Text(
                    "SignOut",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Acme" ,
                    ),
                  ),
                  onTap: () {
                    //redirects user to homepage
                    firebaseAuth.signOut().then((value){
                      Navigator.push(context, MaterialPageRoute(builder: (c)=> const AuthScreen()));
                    });
                  },
                ), // Signout
              ],
            ),
          ),
        ],
      ),
    );
  }
}
