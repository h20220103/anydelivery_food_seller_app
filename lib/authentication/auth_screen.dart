import 'package:anydelivery_seller/authentication/login.dart';
import 'package:anydelivery_seller/authentication/register.dart';
import 'package:flutter/material.dart';


class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2, //1 for login and 1 for signup
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.cyan,
                  Colors.amber,
                ],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp,
              )
            ),
          ), // Contains the formating for the header
          automaticallyImplyLeading: false,
          title: const Text("AnyDelivery_Seller",
          style: TextStyle(
            fontSize: 34,
            color:Colors.white,
            fontFamily: "Lobster",

          ),
          ),
          centerTitle: true,
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.lock,color: Colors.white,),
                text: "Login",
              ),
              Tab(
                icon: Icon(Icons.person,color: Colors.white,),
                text: "Signup",
              )
            ],
            indicatorColor: Colors.white38,
            indicatorWeight: 7,
          ),

        ),
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Colors.amber,
                Colors.cyan,
              ]
            )
          ),
          child: const TabBarView(
            children: [
              LoginScreen(),
              RegisterScreen(),

            ],
          )
        ),
      ),
    );
  }
}
