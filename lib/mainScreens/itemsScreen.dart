import 'package:anydelivery_seller/model/items.dart';
import 'package:anydelivery_seller/uploadScreens/items_upload_screen.dart';
import 'package:anydelivery_seller/widgets/items_design.dart';
import 'package:anydelivery_seller/widgets/text_widget_header.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../global/global.dart';
import '../model/menus.dart';

import '../uploadScreens/menuUploadScreen.dart';
import '../widgets/info_design.dart';
import '../widgets/my_drawer.dart';
import '../widgets/progress_bar.dart';

class ItemsScreen extends StatefulWidget {
  final Menus? model;
  ItemsScreen({this.model});


  @override
  State<ItemsScreen> createState() => _ItemsScreenState();
}

class _ItemsScreenState extends State<ItemsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.cyan,
                  Colors.amber,
                ],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp,
              )
          ),
        ),
        title: Text(
          sharedPreferences!.getString("name")!,
        ),
        centerTitle: true,
        automaticallyImplyLeading: true,
        //Adding a button to send the sender to Upload Menu Screen
        actions: [
          IconButton(
            icon: const Icon(Icons.library_add),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (c) => ItemsUploadScreen(model: widget.model,)));
            },
          ),
        ],
      ),
      drawer: MyDrawer(

      ),
      body: CustomScrollView(
        slivers: [
        SliverPersistentHeader(
          pinned:true,
          delegate: TextWidgetHeader(
            title: "My " + widget.model!.menuTitle.toString() + " Items"),


        ),
          StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance.collection("sellers").doc(sharedPreferences!.getString("uid"))
                .collection("menus").doc(widget.model!.menuID).collection("items").orderBy("title",descending: false).snapshots(),
            builder: (context, snapshot){
              return !snapshot.hasData? SliverToBoxAdapter(
                child: Center(
                  child: circularProgress(),
                ),
              ) : SliverStaggeredGrid.countBuilder(
                crossAxisCount: 2,
                staggeredTileBuilder: (c) => StaggeredTile.fit(1),
                itemBuilder: (context,index){
                  Items model = Items.fromJson(
                    snapshot.data!.docs[index].data()! as Map<String,dynamic>,

                  );
                  return ItemDesignWidget(
                    model: model,
                    context: context,
                  );
                },
                itemCount: snapshot.data!.docs.length,

              );
            },
          ),
      ],
      ),

    );
  }
}
