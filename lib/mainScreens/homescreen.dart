import 'package:anydelivery_seller/authentication/auth_screen.dart';
import 'package:anydelivery_seller/global/global.dart';
import 'package:anydelivery_seller/uploadScreens/menuUploadScreen.dart';
import 'package:anydelivery_seller/widgets/info_design.dart';
import 'package:anydelivery_seller/widgets/my_drawer.dart';
import 'package:anydelivery_seller/widgets/progress_bar.dart';
import 'package:anydelivery_seller/widgets/text_widget_header.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../model/menus.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.cyan,
                  Colors.amber,
                ],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp,
              )
          ),
        ),
        title: Text(
          sharedPreferences!.getString("name")!,
      ),
      centerTitle: true,
        automaticallyImplyLeading: true,
        //Adding a button to send the sender to Upload Menu Screen
        actions: [
          IconButton(
            icon: const Icon(Icons.post_add_outlined),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (c) => const MenusUploadScreen()));
            },
          ),
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverPersistentHeader(
            pinned: true,                                 
            delegate: TextWidgetHeader(title: "My Menus"),
          ),
          StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance.collection("sellers").doc(sharedPreferences!.getString("uid"))
            .collection("menus").orderBy("publishedDate",descending: true).snapshots(),
            builder: (context, snapshot){
              return !snapshot.hasData? SliverToBoxAdapter(
                child: Center(
                  child: circularProgress(),
                ),
                  ) : SliverStaggeredGrid.countBuilder(
                crossAxisCount: 1,
                staggeredTileBuilder: (c) => StaggeredTile.fit(1),
                itemBuilder: (context,index){
                  Menus model = Menus.fromJson(
                     snapshot.data!.docs[index].data()! as Map<String,dynamic>,
                    
                  );
                  return InfoDesignWidget(
                    model: model,
                    context: context,
                  );
                },
                itemCount: snapshot.data!.docs.length,

              );
            },
          ),  // Displaying the menus
        ],
      ),

    );
  }
}
